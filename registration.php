<?php

/**
 * @category  Kowal
 * @package   Kowal_DiscountCodeByLink
 * @author    Sylwester Kowal
 * @copyright Copyright (c) 2023 Sylwester Kowal
 * @license   https://www.gnu.org/licenses/gpl-3.0.en.html GPL v3
 */

\Magento\Framework\Component\ComponentRegistrar::register(
	\Magento\Framework\Component\ComponentRegistrar::MODULE,
	'Kowal_DiscountCodeByLink',
	__DIR__
);

