<?xml version="1.0" ?>

<!--
	@category  Kowal
	@package   Kowal_DiscountCodeByLink
	@author    Sylwester Kowal
	@copyright Copyright (c) 2023 Sylwester Kowal
	@license   https://www.gnu.org/licenses/gpl-3.0.en.html GPL v3
 -->

<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:module:Magento_Config:etc/system_file.xsd">
<system>
	<tab id="kowal" translate="label" sortOrder="999">
		<label>kowal</label>
	</tab>
	<section id="settings" sortOrder="10" showInWebsite="1" showInStore="1" showInDefault="1" translate="label">
		<label>Kod rabatowy przez link</label>
		<tab>kowal</tab>
		<resource>Kowal_DiscountCodeByLink::config_kowal_discountcodebylink</resource>
		<group id="discountcodebylink" sortOrder="10" showInWebsite="1" showInStore="1" showInDefault="1" translate="label">
			<label>Ustawienia adresu URL rabatu</label>

			<field id="enabled" translate="label comment" type="select" sortOrder="60" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Włączone</label>
				<comment>Jeśli moduł URL rabatów jest włączony, będziesz mógł automatycznie stosować kody rabatowe za pośrednictwem adresu URL. Wyłącz tę opcję, jeśli masz uruchomione inne moduły, które implementują podobne zachowanie lub mogą powodować konflikty.</comment>
				<validate>required-entry</validate>
				<source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
			</field>

			<field id="url_param" translate="label comment" type="text" sortOrder="60" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Parametr URL</label>
				<comment>Jest to parametr ciągu zapytania, który będzie zawierał kod kuponu w adresie URL. Na przykład, jeśli ta wartość to "discount", to https://store.url/path/to/page?discount=CODE ustawi kod kuponu na "CODE". Jeśli ta wartość nie zostanie ustawiona, użyjemy zakodowanego domyślnie parametru określonego przez Helper\Cookie::DEFAULT_URL_PARAMETER. Jeśli ścieżka URL jest włączona, będzie to również bit, który powinien zostać dołączony do końca ścieżki URL (przykład: https://store.url/path/to/page/discount/CODE).</comment>
			</field>

			<field id="url_path_enabled" translate="label comment" type="select" sortOrder="61" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Włączona ścieżka URL</label>
				<comment>Jeśli ta opcja jest włączona, kod kuponu można zidentyfikować i zastosować nie tylko za pomocą ciągu zapytania, ale także za pomocą bitu dołączonego do końca ścieżki adresu URL. Na przykład, jeśli parametr URL jest ustawiony na "discount", wówczas URL https://store.url/path/to/page/discount/CODE zastosuje kod rabatowy "CODE" i przekieruje wewnętrznie do https://store.url/path/to/page. Ta funkcja jest nieco eksperymentalna i wymaga pewnego hakowania, więc jeśli ją włączysz, upewnij się, że dokładnie przetestowałeś całą witrynę, aby upewnić się, że nie ma żadnych uszkodzeń, zanim zaczniesz polegać na niej w produkcji. Ta funkcja zachowuje adres URL w przeglądarce i nie przekierowuje. Będzie to działać tylko wtedy, gdy implementacja \Magento \Framework \App \RequestInterface używana do przetwarzania żądania zaimplementowała metodę setPathInfo () (jestem prawie pewien, że będzie to prawdą dla wszystkich, a przynajmniej *prawie* wszystkich).</comment>
				<validate>required-entry</validate>
				<source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
			</field>

			<field id="cookie_lifetime" translate="label comment" type="text" sortOrder="62" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Żywotność plików cookie [dni]</label>
				<comment>Gdy kod kuponu przychodzi za pośrednictwem adresu URL, ustawiamy go w pliku cookie, abyśmy mogli go zapamiętać przez całą sesję. Ta wartość określa, jak długo ten plik cookie powinien pozostać ustawiony, zanim wygaśnie (w dniach). Wartość musi wynosić 0 lub więcej (0 oznacza, że plik cookie pozostanie żywy, dopóki okno przeglądarki lub karta pozostaną otwarte). Jeśli ta wartość nie zostanie ustawiona, użyjemy zakodowanej wartości domyślnej określonej przez Helper\Cookie::COOKIE_LIFETIME.</comment>
				<validate>validate-zero-or-greater</validate>
			</field>

		</group>
		<group id="popup_a" sortOrder="10" showInWebsite="1" showInStore="1" showInDefault="1" translate="label">
			<label>Ustawienia POPUP</label>
			<field id="enabled_popup" translate="label comment" type="select" sortOrder="63" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Włącz POPUP</label>
				<comment>Jeśli włączone wówczas gdy klient będzie opuszczał stronę pojawi mu się POPUP z linkiem generujacym rabat</comment>
				<source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
			</field>
			<field id="enabled_popup_link" translate="label comment" type="select" sortOrder="63" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Włącz Link do strony</label>
				<comment>Jeśli włączone wówczas działa zwykły link</comment>
				<source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
			</field>
			<field id="custom_link" translate="label comment" type="text" sortOrder="63" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Link do podstrony. np.: https://domena.com/regulamin</label>
				<comment>Podaj link do strony internetowej na którą chcesz odesłać klienta.</comment>
				<depends>
					<field id="settings/popup_a/enabled_popup_link">1</field>
				</depends>
			</field>
			<field id="enabled_popup_link_discount" translate="label comment" type="select" sortOrder="63" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Włącz Link z rabatem</label>
				<comment>Jeśli włączone wówczas działa link z kodem rabatowym</comment>
				<source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
			</field>
			<field id="show_once_per_session" translate="label comment" type="select" sortOrder="63" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Wyświetl tylko raz na sesję</label>
				<comment>Jeli ustawimy na Tak wówczas popap będzie się pojawiał tylko raz. Popap pojawi się klientowi gdy ponownie otworzy stronę</comment>
				<source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
				<depends>
					<field id="settings/popup_a/enabled_popup">1</field>
				</depends>
			</field>
			<field id="show_on_delay" translate="label comment" type="select" sortOrder="63" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Wyświetl POPUP po czasie</label>
				<comment>Jeśli włączone wówczas gdy klient będzie przebywał na stronie określony czas, pojawi mu się POPUP z linkiem generujacym rabat</comment>
				<source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
				<depends>
					<field id="settings/popup_a/enabled_popup">1</field>
				</depends>
			</field>
			<field id="delay_time_in_sec" translate="label comment" type="text" sortOrder="63" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Czas wyświetlenia POPUP</label>
				<comment>Podaj czas w sek. po jakim pojawi się POPUP.</comment>
				<depends>
					<field id="settings/popup_a/show_on_delay">1</field>
				</depends>
			</field>

			<field id="popup_cupon_code" translate="label comment" type="text" sortOrder="64" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Kod rabatowy dla POPUP</label>
				<comment>Podaj kod rabatowy, który wyświetli się klientowi w okienku POPUP gdy będzie opuszczał stronę.</comment>
				<depends>
					<field id="settings/popup_a/enabled_popup_link_discount">1</field>
				</depends>
			</field>

			<field id="popup_image" showInDefault="1" showInStore="1" showInWebsite="1" sortOrder="65" translate="label" type="image">
				<label>Wczytaj baner</label>
				<comment/>
				<base_url type="media" scope_info="1">popup_baner</base_url>
				<backend_model>Kowal\DiscountCodeByLink\Model\Config\Backend\Baner</backend_model>
				<depends>
					<field id="settings/popup_a/enabled_popup">1</field>
				</depends>
			</field>

			<field id="popup_cupon_content" translate="label comment" type="text" sortOrder="66" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Treść na POPUP</label>
				<comment>Podaj opis promocji dla linka</comment>
				<depends>
					<field id="settings/popup_a/enabled_popup">1</field>
				</depends>
			</field>

			<field id="enable_cupon_code_input" translate="label comment" type="select" sortOrder="63" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Wyświetl opis i pole z kodem</label>
				<comment>Jeśli włączone wówczas pojawi się opis oraz pole z którego można skopiowac kod rabatowy</comment>
				<source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
				<depends>
					<field id="settings/popup_a/enabled_popup_link_discount">1</field>
				</depends>
			</field>

			<field id="popup_cupon_content_code" translate="label comment" type="text" sortOrder="67" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Treść na POPUP do skopiowania kodu</label>
				<comment>Podaj opis promocji dla kodu rabatowego, który można skopiować</comment>
				<depends>
					<field id="settings/popup_a/enabled_popup_link_discount">1</field>
				</depends>
			</field>

			<field id="popup_width" translate="label comment" type="text" sortOrder="68" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Szerokość POPUPA</label>
				<comment>Podaj Szerokość w piksalch np.: 400px</comment>
				<depends>
					<field id="settings/popup_a/enabled_popup">1</field>
				</depends>
			</field>
			<field id="popup_height" translate="label comment" type="text" sortOrder="69" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Wysokość POPUPA</label>
				<comment>Podaj Wysokość w piksalch np.: 400px</comment>
				<depends>
					<field id="settings/popup_a/enabled_popup">1</field>
				</depends>
			</field>

			<field id="popup_bg" translate="label comment" type="text" sortOrder="69" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Kolor tła POPUPa</label>
				<comment>Podaj kolor tła okienka np.: #FFFFFF</comment>
				<depends>
					<field id="settings/popup_a/enabled_popup">1</field>
				</depends>
			</field>
			<field id="popup_content_title" translate="label comment" type="text" sortOrder="70" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Kolor nagłówka w popup</label>
				<comment>Podaj kolor nagłowka w okienku np.: #f21b1b</comment>
				<depends>
					<field id="settings/popup_a/enabled_popup">1</field>
				</depends>
			</field>
			<field id="popup_bg_bt" translate="label comment" type="text" sortOrder="71" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Kolor tła przycisku</label>
				<comment>Podaj kolor tła dla przycisku np.: #70bb39</comment>
				<depends>
					<field id="settings/popup_a/enabled_popup">1</field>
				</depends>
			</field>

			<field id="popup_text_bt" translate="label comment" type="text" sortOrder="72" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Kolor tekstu na przycisku</label>
				<comment>Podaj kolor tekstu dla przycisku np.: #ffffff</comment>
				<depends>
					<field id="settings/popup_a/enabled_popup">1</field>
				</depends>
			</field>

			<field id="popup_content_text" translate="label comment" type="text" sortOrder="73" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Kolor tekstu w popup</label>
				<comment>Podaj kolor tekstu w okienku np.: #444444</comment>
				<depends>
					<field id="settings/popup_a/enabled_popup">1</field>
				</depends>
			</field>

			<field id="popup_content_text_size" translate="label comment" type="text" sortOrder="73" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Wielkość tekstu w popup</label>
				<comment>Podaj wielokość tekstu w okienku np.: 16px</comment>
				<depends>
					<field id="settings/popup_a/enabled_popup">1</field>
				</depends>
			</field>

			<field id="popup_content_title_size" translate="label comment" type="text" sortOrder="73" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Wielkość nagłówka</label>
				<comment>Podaj wielokość nagłówka w okienku np.: 36px</comment>
				<depends>
					<field id="settings/popup_a/enabled_popup">1</field>
				</depends>
			</field>

			<field id="popup_content_bt_size" translate="label comment" type="text" sortOrder="73" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Wielkość tekstu przycisku</label>
				<comment>Podaj wielokość czcionki dla przycisku np.: 16px</comment>
				<depends>
					<field id="settings/popup_a/enabled_popup">1</field>
				</depends>
			</field>
			<field id="popup_bt_text" translate="label comment" type="text" sortOrder="73" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Treść przycisku</label>
				<comment>Podaj tekst w przycisku np.: ODBIERZ SWÓJ RABAT</comment>
				<depends>
					<field id="settings/popup_a/enabled_popup">1</field>
				</depends>
			</field>
			<field id="popup_title_text" translate="label comment" type="text" sortOrder="73" showInDefault="1" showInWebsite="1" showInStore="1">
				<label>Treść nagłowka</label>
				<comment>Podaj tekst dla nagłówka np.: ZATRZYMAJ SIĘ!</comment>
				<depends>
					<field id="settings/popup_a/enabled_popup">1</field>
				</depends>
			</field>
		</group>
	</section>
</system>
</config>