<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\DiscountCodeByLink\Block;

class Popup extends \Magento\Framework\View\Element\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\UrlInterface                  $urlInterface,
        \Kowal\DiscountCodeByLink\Helper\Config          $config,
        \Magento\Framework\Filesystem\DirectoryList      $directory_,
        \Kowal\DiscountCodeByLink\Helper\Cookie          $cookieHelper,
        array                                            $data = []
    )
    {
        $this->urlInterface = $urlInterface;
        $this->config = $config;
        $this->directoryList = $directory_;
        $this->cookieHelper = $cookieHelper;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getCuponCode()
    {
        return $this->config->getUrlCuponCode();
    }

    public function getCurrentUrl()
    {
        return $this->urlInterface->getCurrentUrl();
    }

    public function isEnabledPopup()
    {
        $coupon_coockie = $this->cookieHelper->getCookie();
        $coupon_set = $this->getCuponCode();
        if($coupon_coockie == $coupon_set) return false; // jeśli już kupon jest w sesji to nie wyświetlaj popup
        return $this->config->isEnabledPopup();
    }

    public function isEnabledPopupLink()
    {
        return $this->config->isEnabledPopupLink();
    }
    public function isEnabledPopupLinkDiscount()
    {
        return $this->config->isEnabledPopupLinkDiscount();
    }

    public function isShowOnDelayEnabled()
    {
        return ($this->config->isShowOnDelayEnabled()) ? 1 : 0;
    }

    public function isShowOncePerSession()
    {
        return ($this->config->isShowOncePerSession()) ? 1 : 0;
    }

    public function isEnabledCuponCodeInput()
    {
        return $this->config->isEnabledCuponCodeInput();
    }

    public function getPopupDelay()
    {
        return $this->config->getPopupDelay();
    }

    public function getUrlParameter()
    {
        return $this->config->getUrlParameter();
    }

    public function getPopupContentBefore()
    {
        return $this->config->getPopupContentBefore();
    }

    public function getPopupContentAfter()
    {
        return $this->config->getPopupContentAfter();
    }

    public function getBanerUrl()
    {
        $this->pupfolder = $this->directoryList->getPath('pub');
        return DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'popup_baner' . DIRECTORY_SEPARATOR . $this->config->getPopupBaner();
    }

    public function getPopupBg()
    {
        return $this->config->getPopupBG();
    }

    public function getPopupWidth()
    {
        return $this->config->getPopupWidth();
    }

    public function getPopupHeight()
    {
        return $this->config->getPopupHeight();
    }

    public function getPopupContentTitleColor()
    {
        return $this->config->getPopupContentTitleColor();
    }

    public function getPopupBgBtColor()
    {
        return $this->config->getPopupBgBtColor();
    }

    public function getPopupTextBtColor()
    {
        return $this->config->getPopupTextBtColor();
    }

    public function getPopupContentTextColor()
    {
        return $this->config->getPopupContentTextColor();
    }

    public function getPopupContentTextSize()
    {
        return $this->config->getPopupContentTextSize();
    }

    public function getPopupContentTitleSize()
    {
        return $this->config->getPopupContentTitleSize();
    }

    public function getPopupContentBtSize()
    {
        return $this->config->getPopupContentBtSize();
    }

    public function getPopupBtText()
    {
        return $this->config->getPopupBtText();
    }

    public function getPopupTitleText()
    {
        return $this->config->getPopupTitleText();
    }

    public function getCookieLifetime()
    {
        return $this->config->getCookieLifetime();
    }

    public function getCustomLink()
    {
        return $this->config->getCustomLink();
    }
}