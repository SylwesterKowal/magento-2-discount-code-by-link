<?php

/**
 * @category  Kowal
 * @package   Kowal_DiscountCodeByLink
 * @author    Sylwester Kowal
 * @copyright Copyright (c) 2023 Sylwester Kowal
 * @license   https://www.gnu.org/licenses/gpl-3.0.en.html GPL v3
 */

namespace Kowal\DiscountCodeByLink\Observer\Discountcodebylink;

class ControllerFrontSendResponseBefore implements \Magento\Framework\Event\ObserverInterface {

	/**
	 * @var \Kowal\DiscountCodeByLink\Helper\Config
	 */
	private $config;

	/**
	 * @var \Kowal\DiscountCodeByLink\Helper\Cookie
	 */
	private $cookieHelper;

	/**
	 * @var \Magento\Framework\Registry $registry
	 */
	private $registry;

	/**
	 * @var \Magento\Framework\Message\ManagerInterface
	 */
	private $messageManager;

	/************************************************************************/

	/**
	 * Constructor
	 *
	 * @param \Kowal\DiscountCodeByLink\Helper\Config $config
	 * @param \Kowal\DiscountCodeByLink\Helper\Cookie $cookieHelper
	 * @param \Magento\Framework\Registry $registry
	 * @param \Magento\Framework\Message\ManagerInterface $messageManager
	 */
	public function __construct(
		\Kowal\DiscountCodeByLink\Helper\Config $config,
		\Kowal\DiscountCodeByLink\Helper\Cookie $cookieHelper,
		\Magento\Framework\Registry $registry,
		\Magento\Framework\Message\ManagerInterface $messageManager
	) {
		$this->config = $config;
		$this->cookieHelper = $cookieHelper;
		$this->registry = $registry;
		$this->messageManager = $messageManager;
	}

	/************************************************************************/

	/**
	 * Jeśli w adresie URL ustawiono prawidłowy kod kuponu, zapisujemy tutaj plik cookie z tą wartością
     *, abyśmy mogli go zapamiętać, gdy nadejdzie czas realizacji transakcji. Oryginalnie,
     * próbowałem ustawić tę wartość w
     * Plugin\FrontControllerInterface::beforeDispatch(), ale po dziwnych problemach i prześledzeniu kodu
     * dziwnych problemów i prześledzeniu kodu w rdzeniu Magento, odkryłem w
     * Magento\Framework\App\Http::launch(), że powinienem nasłuchiwać dla funkcji
     * controller_front_send_response_before i tam ustawić ciasteczka,
     * ponieważ nagłówki plików cookie nie mogą być ustawione, dopóki żądanie nie zostanie
     * wysłane, a odpowiedź została w pełni wyrenderowana. Spędziłem wiele godzin
     * wyrywając sobie włosy z głowy, aby to rozgryźć...
	 *
	 * @param \Magento\Framework\Event\Observer $observer
	 *
	 * @return void
	 */
	public function execute(\Magento\Framework\Event\Observer $observer): void {

		if ($this->config->isEnabled()) {

			$coupon = $this->registry->registry('kowal_discountcodebylink_coupon');
			$message = $this->registry->registry('kowal_discountcodebylink_message');

			if ($coupon) {
				$this->cookieHelper->setCookie($coupon);
			}

			if ($message) {
				if ($message['error']) {
					$this->messageManager->addError($message['message']);
				} else {
					$this->messageManager->addSuccess($message['message']);
				}
			}
		}
	}
}

