<?php

/**
 * @category  Kowal
 * @package   Kowal_DiscountCodeByLink
 * @author    Sylwester Kowal
 * @copyright Copyright (c) 2023 Sylwester Kowal
 * @license   https://www.gnu.org/licenses/gpl-3.0.en.html GPL v3
 */

namespace Kowal\DiscountCodeByLink\Observer\Discountcodebylink;

class SalesOrderPlaceAfter implements \Magento\Framework\Event\ObserverInterface {

	/**
	 * @var \Kowal\DiscountCodeByLink\Helper\Config
	 */
	private $config;

	/**
	 * @var \Kowal\DiscountCodeByLink\Helper\Cookie
	 */
	private $cookieHelper;

	/************************************************************************/

	/**
	 * Constructor
	 *
	 * @param \Kowal\DiscountCodeByLink\Helper\Cookie $cookieHelper
	 */
	public function __construct(
		\Kowal\DiscountCodeByLink\Helper\Config $config,
		\Kowal\DiscountCodeByLink\Helper\Cookie $cookieHelper
	) {
		$this->config = $config;
		$this->cookieHelper = $cookieHelper;
	}

	/************************************************************************/

	/**
	 * Jeśli kod kuponu został ustawiony w adresie URL w dowolnym momencie sesji
     * i zamówienie zostało pomyślnie złożone, powinniśmy go usunąć, aby uniknąć
     * automatycznego zastosowania go po raz drugi (użytkownik będzie musiał
     * albo ponownie wprowadzić kod ręcznie, albo ponownie przejść do adresu URL
     * adres URL specyficzny dla kuponu.)
	 *
	 * @param \Magento\Framework\Event\Observer $observer
	 *
	 * @return void
	 */
	public function execute(\Magento\Framework\Event\Observer $observer): void {

		// Once we've placed an order, we should delete the coupon cookie so
		// that the user will have to add one again if they wish to place
		// another order
		if ($this->config->isEnabled()) {
			$this->cookieHelper->deleteCookie();
		}
	}
}

