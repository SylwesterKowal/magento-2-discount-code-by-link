<?php

/**
 * @category  Kowal
 * @package   Kowal_DiscountCodeByLink
 * @author    Sylwester Kowal
 * @copyright Copyright (c) 2023 Sylwester Kowal
 * @license   https://www.gnu.org/licenses/gpl-3.0.en.html GPL v3
 */

namespace Kowal\DiscountCodeByLink\Observer\Discountcodebylink;

class CheckoutCartSaveAfter implements \Magento\Framework\Event\ObserverInterface {

	/**
	 * @var \Kowal\DiscountCodeByLink\Helper\Config
	 */
	private $config;

	/**
	 * @var \Kowal\DiscountCodeByLink\Helper\Cookie
	 */
	private $cookieHelper;

	/**
	 * @var \Kowal\DiscountCodeByLink\Helper\Cart
	 */
	private $cartHelper;

	/************************************************************************/

	/**
	 * Constructor
	 *
	 * @param \Kowal\DiscountCodeByLink\Helper\Config $config
	 * @param \Kowal\DiscountCodeByLink\Helper\Cookie $cookieHelper
	 * @param \Kowal\DiscountCodeByLink\Helper\Cart $cartHelper
	 */
	public function __construct(
		\Kowal\DiscountCodeByLink\Helper\Config $config,
		\Kowal\DiscountCodeByLink\Helper\Cookie $cookieHelper,
		\Kowal\DiscountCodeByLink\Helper\Cart $cartHelper
	) {
		$this->config = $config;
		$this->cookieHelper = $cookieHelper;
		$this->cartHelper = $cartHelper;
	}

	/************************************************************************/

	/**
	 * Jeśli kod kuponu został ustawiony w adresie URL w dowolnym momencie sesji,
     * zastosuje go zaraz po utworzeniu koszyka i ponownie zastosuje za każdym razem, gdy zostanie on
     * aktualizowany, aby utrzymać aktualną cenę całkowitą.
	 *
	 * @param \Magento\Framework\Event\Observer $observer
	 *
	 * @return void
	 */
	public function execute(\Magento\Framework\Event\Observer $observer): void {

		if ($this->config->isEnabled()) {

			$coupon = $this->cookieHelper->getCookie();

			if ($coupon) {

				$cart = $observer->getData('cart');

				if ($cart) {
					$this->cartHelper->applyCoupon($cart->getQuote(), $coupon);
				}
			}
		}
	}
}

