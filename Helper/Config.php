<?php

/**
 * @category  Kowal
 * @package   Kowal_DiscountCodeByLink
 * @author    Sylwester Kowal
 * @copyright Copyright (c) 2023 Sylwester Kowal
 * @license   https://www.gnu.org/licenses/gpl-3.0.en.html GPL v3
 */

namespace Kowal\DiscountCodeByLink\Helper;

class Config extends \Magento\Framework\App\Helper\AbstractHelper {

	/**
	 * Parametr URL, którego powinniśmy szukać, aby ustawić aktualny kod kuponu.
	 *
	 * @var DEFAULT_URL_PARAMETER Parametr URL zawierający kod kuponu
	 */
	public const DEFAULT_URL_PARAMETER = 'discount';
	public const DEFAULT_URL_CUPONCODE = 'popup_cupon_code';

	/**
	 * Gdy kod jest dostarczany za pośrednictwem adresu URL, ustawiany jest plik cookie, który pozwala nam
     * zapamiętać go podczas sesji.
	 *
	 * @var COUPON_COOKIE_NAME Nazwa pliku cookie przechowującego kod rabatowy
	 */
	public const COUPON_COOKIE_NAME = 'discount_coupon_url_code';

	/**
	 * Jest to czas, przez jaki sesja przeglądarki powinna pamiętać ostatni kod kuponu
     *, który został dostarczony za pośrednictwem adresu URL w sekundach. Domyślna wartość 0 oznacza
     * plik cookie będzie trwał tak długo, jak sesja (tj. do momentu zamknięcia karty przeglądarki
     * lub okno przeglądarki zostanie zamknięte.)
	 *
	 * @var DEFAULT_COOKIE_LIFETIME Domyślny czas życia pliku cookie w sekundach
	 */
	public const DEFAULT_COOKIE_LIFETIME = 0;

	/**
	 * @var ENABLED_CONFIG_PATH Czy moduł jest włączony
	 */
	public const ENABLED_CONFIG_PATH = 'settings/discountcodebylink/enabled';


	public const ENABLED_CONFIG_POPUP = 'settings/popup_a/enabled_popup';
    public const ENABLED_POPUP_LINK_PATH = 'settings/popup_a/enabled_popup_link';
    public const CUSTOM_LINK = 'settings/popup_a/custom_link';
    public const ENABLED_POPUP_LINK_DISCOUNT_PATH = 'settings/popup_a/enabled_popup_link_discount';
	public const ENABLED_SHOW_ON_DELAY = 'settings/popup_a/show_on_delay';
	public const ENABLED_SHOW_ONCE_PER_SESSION = 'settings/popup_a/show_once_per_session';
	public const ENABLED_CUPON_CODE_INPUT = 'settings/popup_a/enable_cupon_code_input';
    public const POPUP_CUPON_CODE = 'settings/popup_a/popup_cupon_code';
	public const POPUP_DELAY_IN_SEC = 'settings/popup_a/delay_time_in_sec';
	public const POPUP_CONTENT_BEFORE = 'settings/popup_a/popup_cupon_content';
	public const POPUP_CONTENT_AFTER = 'settings/popup_a/popup_cupon_content_code';
	public const POPUP_BANER = 'settings/popup_a/popup_image';
	public const POPUP_BG = 'settings/popup_a/popup_bg';
	public const POPUP_WIDTH = 'settings/popup_a/popup_width';
	public const POPUP_HEIGHT = 'settings/popup_a/popup_height';
	public const POPUP_CONTENT_TITLE_COLOR = 'settings/popup_a/popup_content_title';
	public const POPUP_BG_BT_COLOR = 'settings/popup_a/popup_bg_bt';
	public const POPUP_TEXT_BT_COLOR = 'settings/popup_a/popup_text_bt';
	public const POPUP_CONTENT_TEXT_COLOR = 'settings/popup_a/popup_content_text';
	public const POPUP_CONTENT_TEXT_SIZE = 'settings/popup_a/popup_content_text_size';
	public const POPUP_CONTENT_TITLE_SIZE = 'settings/popup_a/popup_content_title_size';
	public const POPUP_CONTENT_BT_SIZE = 'settings/popup_a/popup_content_bt_size';
	public const POPUP_BT_TEXT = 'settings/popup_a/popup_bt_text';
	public const POPUP_TITLE_TEXT = 'settings/popup_a/popup_title_text';

	/**
	 * @var URL_PARAMETER_CONFIG_PATH Parametr GET, który powinien ustawić kod kuponu
	 */
	public const URL_PARAMETER_CONFIG_PATH = 'settings/discountcodebylink/url_param';

	/**
	 * @var PATH_ENABLED_PATH Czy stosować kody rabatowe za pośrednictwem ścieżki URL?
	 */
	public const URL_PATH_ENABLED_PATH = 'settings/discountcodebylink/url_path_enabled';


	/**
	 * @var COOKIE_LIFETIME_CONFIG_PATH Jak długo powinien działać plik cookie
	 */
	public const COOKIE_LIFETIME_CONFIG_PATH = 'settings/discountcodebylink/cookie_lifetime';

	/************************************************************************/

	/**
	 * Zwraca, czy moduł jest włączony.
	 *
	 * @param string|int $scope
	 *
	 * @return bool
	 */
	public function isEnabled(): bool {

		return $this->scopeConfig->isSetFlag(self::ENABLED_CONFIG_PATH,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

    public function isEnabledPopup(): bool {

		return $this->scopeConfig->isSetFlag(self::ENABLED_CONFIG_POPUP,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

    public function isEnabledCuponCodeInput(): bool {

		return $this->scopeConfig->isSetFlag(self::ENABLED_CUPON_CODE_INPUT,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

	/************************************************************************/

	/**
	 * Zwraca informację, czy funkcja ścieżki URL jest włączona.
	 *
	 * @param string|int $scope
	 *
	 * @return bool
	 */
	public function isUrlPathEnabled(): bool {

		return $this->scopeConfig->isSetFlag(self::URL_PATH_ENABLED_PATH,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

    public function isEnabledPopupLink(): bool {

        return $this->scopeConfig->isSetFlag(self::ENABLED_POPUP_LINK_PATH,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function isEnabledPopupLinkDiscount(): bool {

        return $this->scopeConfig->isSetFlag(self::ENABLED_POPUP_LINK_DISCOUNT_PATH,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function isShowOnDelayEnabled(): bool {

		return $this->scopeConfig->isSetFlag(self::ENABLED_SHOW_ON_DELAY,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

    public function isShowOncePerSession(): bool {

        return $this->scopeConfig->isSetFlag(self::ENABLED_SHOW_ONCE_PER_SESSION,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

	/************************************************************************/

	/**
	 *
	 * @param string|int $scope
	 *
	 * @return string
	 */
	public function getUrlParameter(): string {
		$value = $this->scopeConfig->getValue(self::URL_PARAMETER_CONFIG_PATH,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		return is_null($value) || '' === $value ? '' : $value;
	}
    public function getUrlCuponCode(): string {
        $value = $this->scopeConfig->getValue(self::POPUP_CUPON_CODE,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? '' : $value;
	}
    public function getPopupContentBefore(): string {
        $value = $this->scopeConfig->getValue(self::POPUP_CONTENT_BEFORE,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? '' : $value;
	}
    public function getPopupContentAfter(): string {
        $value = $this->scopeConfig->getValue(self::POPUP_CONTENT_AFTER,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? '' : $value;
	}
    public function getPopupBaner(): string {
        $value = $this->scopeConfig->getValue(self::POPUP_BANER,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? '' : $value;
	}
    public function getPopupBG(): string {
        $value = $this->scopeConfig->getValue(self::POPUP_BG,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? '' : $value;
    }
    public function getPopupWidth(): string {
        $value = $this->scopeConfig->getValue(self::POPUP_WIDTH,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? '' : $value;
    }
    public function getPopupHeight(): string {
        $value = $this->scopeConfig->getValue(self::POPUP_HEIGHT,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? '' : $value;
    }
    public function getPopupContentTitleColor(): string {
        $value = $this->scopeConfig->getValue(self::POPUP_CONTENT_TITLE_COLOR,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? '' : $value;
    }
    public function getPopupBgBtColor(): string {
        $value = $this->scopeConfig->getValue(self::POPUP_BG_BT_COLOR,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? '' : $value;
    }
    public function getPopupTextBtColor(): string {
        $value = $this->scopeConfig->getValue(self::POPUP_TEXT_BT_COLOR,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? '' : $value;
    }
    public function getPopupContentTextColor(): string {
        $value = $this->scopeConfig->getValue(self::POPUP_CONTENT_TEXT_COLOR,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? '' : $value;
    }
    public function getPopupContentTextSize(): string {
        $value = $this->scopeConfig->getValue(self::POPUP_CONTENT_TEXT_SIZE,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? '' : $value;
    }
    public function getPopupContentTitleSize(): string {
        $value = $this->scopeConfig->getValue(self::POPUP_CONTENT_TITLE_SIZE,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? '' : $value;
    }
    public function getPopupContentBtSize(): string {
        $value = $this->scopeConfig->getValue(self::POPUP_CONTENT_BT_SIZE,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? '' : $value;
    }
    public function getPopupBtText(): string {
        $value = $this->scopeConfig->getValue(self::POPUP_BT_TEXT,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? '' : $value;
    }
    public function getPopupTitleText(): string {
        $value = $this->scopeConfig->getValue(self::POPUP_TITLE_TEXT,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? '' : $value;
    }
    public function getPopupDelay(): string {
        $value = $this->scopeConfig->getValue(self::POPUP_DELAY_IN_SEC,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? 30 : $value;
    }
    public function getCustomLink(): string {
        $value = $this->scopeConfig->getValue(self::CUSTOM_LINK,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return is_null($value) || '' === $value ? '#' : $value;
    }
	/************************************************************************/

	/**
	 *
	 * @param string|int $scope
	 *
	 * @return int
	 */
	public function getCookieLifetime(): int {

		$value = $this->scopeConfig->getValue(self::COOKIE_LIFETIME_CONFIG_PATH,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		return (int) (is_null($value) || '' === $value ? 0 : $value);
	}

	/************************************************************************/

	public function getCookieName(): string {

		return self::COUPON_COOKIE_NAME;
	}
}

