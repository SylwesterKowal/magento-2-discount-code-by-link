<?php

/**
 * @category  Kowal
 * @package   Kowal_DiscountCodeByLink
 * @author    Sylwester Kowal
 * @copyright Copyright (c) 2023 Sylwester Kowal
 * @license   https://www.gnu.org/licenses/gpl-3.0.en.html GPL v3
 */

namespace Kowal\DiscountCodeByLink\Helper;

class Cookie {

	/**
	 * @var \Magento\Framework\Stdlib\CookieManagerInterface
	 */
	private $cookieManager;

	/**
	 * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
	 */
	private $cookieMetadataFactory;

	/**
	 * @var \Magento\Framework\Session\SessionManagerInterface
	 */
	private $sessionManager;

	/**
	 * @var \Kowal\DiscountCodeByLink\Helper\Config
	 */
	private $config;

	/************************************************************************/

	public function __construct(
		\Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
		\Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
		\Magento\Framework\Session\SessionManagerInterface $sessionManager,
		\Kowal\DiscountCodeByLink\Helper\Config $config
	) {
		$this->cookieManager = $cookieManager;
		$this->cookieMetadataFactory = $cookieMetadataFactory;
		$this->sessionManager = $sessionManager;
		$this->config = $config;
	}

	/************************************************************************/

	/**
	 * Pobiera aktualnie zastosowany kod kuponu sesji lub pusty .
	 *
	 * @return string|null
	 */
	public function getCookie(): ?string {

		$value = $this->cookieManager->getCookie($this->config->getCookieName());
		return $value ? $value : null;
	}

	/************************************************************************/

	/**
	 * Ustawia plik cookie z kodem kuponu, abyśmy mogli go zapamiętać w bieżącej sesji.
	 *
	 * @param $value
	 *
	 * @return void
	 */
	public function setCookie($value): void {

		$cookieLifetime = $this->config->getCookieLifetime();
		$metadata = $this->cookieMetadataFactory->createPublicCookieMetadata();

		$metadata->setPath($this->sessionManager->getCookiePath());
		$metadata->setDomain($this->sessionManager->getCookieDomain());
		$metadata->setHttpOnly(false);

		if ($cookieLifetime > 0) {
			$metadata->setDuration($cookieLifetime);
		}

		$this->cookieManager->setPublicCookie(
			$this->config->getCookieName(),
			$value,
			$metadata
		);
	}



	/************************************************************************/

	/**
	 * Usuwa plik cookie z kodem kuponu.
	 *
	 * @return void
	 */
	public function deleteCookie(): void {

		$metadata = $this->cookieMetadataFactory->createPublicCookieMetadata();

		$metadata->setPath($this->sessionManager->getCookiePath());
		$metadata->setDomain($this->sessionManager->getCookieDomain());
		$metadata->setHttpOnly(false);

		$this->cookieManager->deleteCookie($this->config->getCookieName(), $metadata);
	}
}

